import argparse
import logging

# memoizer for smarter counting
memoized = {}


def catalanNum(n):
    if n <= 1:
        return 1
    res = 0
    for i in range(0, n):
        res += catalanNum(i) * catalanNum(n - i - 1)
    return res


def smart_catalanNum(n):
    if n <= 1:
        return 1
    n_mem = memoized.get(n, False)
    if n_mem:
        return n_mem
    res = 0
    for i in range(0, n):
        res += smart_catalanNum(i) * smart_catalanNum(n - i - 1)
    memoized[n] = res
    return res


def main():
    parser = argparse.ArgumentParser(description="Binary Search Tree Counter")
    parser.add_argument(
        "num", type=int,
        help="Input number of nodes to calculate number of possible Binary Search Trees"
    )
    parser.add_argument("--smart", dest='algo', action="store_true")
    args = parser.parse_args()
    if args.algo:
        logging.info("Running smarter algo")
        logging.info(smart_catalanNum(args.num))
    else:
        logging.info("Running vanilla algo")
        logging.info(catalanNum(args.num))


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()
