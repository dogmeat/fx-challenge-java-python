package com.fxclub.treecounter;

/**
 * Tree Counter JAVA8 implementation
 *
 */


public class App 
{
    public static void main(String[] args)
    {
        int num = Integer.parseInt(args[0]);
        TreeCounter tc = new TreeCounter();
        System.out.printf("Number of binary search trees for %d nodes is %d.\n", num, tc.catalanNum(num));
    }
}
