package com.fxclub.treecounter;

import java.util.stream.IntStream;

public class TreeCounter {

    public int catalanNum(int n) {
        if (n <= 1){
            return 1;
        }
        int res = 0;
        res = IntStream.range(0, n).map(i -> catalanNum(i) * catalanNum(n - i - 1)).sum();
        return res;
    }
}
