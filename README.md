# Binary Search Tree Count JAVA and Python solutions

I put both solutions (Java and Python).

## Java solution
Is basically the same "Catalan number" solution as is Scala.
It has been built from maven's simple java archetype and manually edited to make Java8 a default compiler version, and imported a jar package plugin also.

To build and test the app do:

	cd treecounter
	mvn package
	
After the successfull build and tests pass, run the app with:

	java -jar target/treecounter-1.0-SNAPSHOT.jar <integer-to-test>

## Python solution

Python solution has a regular catalan solution, but also an improved one. Both are implemented and can be set at runtime with a `--smart` flag.

	cd python

Catalan solution:

	python treecounter.py <integer-to-calculate>

Improved solution
	
	python treecounter.py <integer-to-calculate> --smart

The idea at it came to me while i tried to find a better way to do it than to use recursion, because of the large number of repeated calculations. I tried solving that using Dynamic Programming, but the existing catalan number was clean there, so i started memoizing the already calculated sub solutions, and timed the execution compared to the vanilla algo's ones. There was a significant speedup noticed.

	%timeit treecounter.catalanNum(12)
	72.9 ms ± 6.52 ms per loop (mean ± std. dev. of 7 runs, 10 loops each)

	%timeit treecounter.smart_catalanNum(12)
	1.95 µs ± 105 ns per loop (mean ± std. dev. of 7 runs, 1000000 loops each)

The results are in the accompanying Jupyter notebook.